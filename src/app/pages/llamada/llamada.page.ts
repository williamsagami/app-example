import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { ModalController, NavParams } from '@ionic/angular';
import { TraductorService } from 'src/app/services/traductor.service';

@Component({
  selector: 'app-llamada',
  templateUrl: './llamada.page.html',
  styleUrls: ['./llamada.page.scss'],
})
export class LlamadaPage implements OnInit {

  phoneNumber: string;

  constructor(private modalController: ModalController, private navParams: NavParams, private callNumber: CallNumber, private translate: TraductorService) { 
    this.phoneNumber = this.navParams.get('number');
  }

  ngOnInit() {
  }

  phoneCall() {
	  this.callNumber.callNumber(this.phoneNumber, true)
	  .then(res => console.log('Launched dialer!', res))
    .catch(err => console.log('Error launching dialer', err));
  }
  
  closeCall(){
	  this.modalController.dismiss();
  }

}
