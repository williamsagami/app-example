import { Component, OnInit } from '@angular/core';
import { ModalController } from "@ionic/angular";
import { ImagenPage } from "../imagen/imagen.page";
import { TraductorService } from 'src/app/services/traductor.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-galeria',
  templateUrl: './galeria.page.html',
  styleUrls: ['./galeria.page.scss'],
})
export class GaleriaPage implements OnInit {

  constructor(private modalController: ModalController, private translate: TraductorService, private data: DataService) { }

  ngOnInit() {
  }

  async openImage(i: number)
	{
    const modal = await this.modalController.create({
      component: ImagenPage,
      cssClass: 'modal-transparency',
      componentProps : {
        index: i+1
      }
    });
    await modal.present();
	}
}
