(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["galeria-galeria-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/galeria/galeria.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/galeria/galeria.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title style=\"color: #fff;\">{{translate.gallery}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"pattern-bg\">\n  <ion-grid style=\"padding: 0px;\">\n    <ion-row>\n      <ion-col size=\"12\" *ngFor=\"let image of data.images; index as i\">\n        <ion-card class=\"animate__animated animate__fadeInUp cool-image\" button (click) = \"openImage(i)\">\n          <img class=\"image\" [src]=\"image\">\n        </ion-card>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/galeria/galeria-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/galeria/galeria-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: GaleriaPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleriaPageRoutingModule", function() { return GaleriaPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _galeria_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./galeria.page */ "./src/app/pages/galeria/galeria.page.ts");




var routes = [
    {
        path: '',
        component: _galeria_page__WEBPACK_IMPORTED_MODULE_3__["GaleriaPage"]
    }
];
var GaleriaPageRoutingModule = /** @class */ (function () {
    function GaleriaPageRoutingModule() {
    }
    GaleriaPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], GaleriaPageRoutingModule);
    return GaleriaPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/galeria/galeria.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/galeria/galeria.module.ts ***!
  \*************************************************/
/*! exports provided: GaleriaPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleriaPageModule", function() { return GaleriaPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _galeria_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./galeria-routing.module */ "./src/app/pages/galeria/galeria-routing.module.ts");
/* harmony import */ var _galeria_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./galeria.page */ "./src/app/pages/galeria/galeria.page.ts");







var GaleriaPageModule = /** @class */ (function () {
    function GaleriaPageModule() {
    }
    GaleriaPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _galeria_routing_module__WEBPACK_IMPORTED_MODULE_5__["GaleriaPageRoutingModule"]
            ],
            declarations: [_galeria_page__WEBPACK_IMPORTED_MODULE_6__["GaleriaPage"]]
        })
    ], GaleriaPageModule);
    return GaleriaPageModule;
}());



/***/ }),

/***/ "./src/app/pages/galeria/galeria.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/galeria/galeria.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2dhbGVyaWEvZ2FsZXJpYS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/galeria/galeria.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/galeria/galeria.page.ts ***!
  \***********************************************/
/*! exports provided: GaleriaPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "GaleriaPage", function() { return GaleriaPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _imagen_imagen_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../imagen/imagen.page */ "./src/app/pages/imagen/imagen.page.ts");
/* harmony import */ var src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/traductor.service */ "./src/app/services/traductor.service.ts");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");






var GaleriaPage = /** @class */ (function () {
    function GaleriaPage(modalController, translate, data) {
        this.modalController = modalController;
        this.translate = translate;
        this.data = data;
    }
    GaleriaPage.prototype.ngOnInit = function () {
    };
    GaleriaPage.prototype.openImage = function (i) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _imagen_imagen_page__WEBPACK_IMPORTED_MODULE_3__["ImagenPage"],
                            cssClass: 'modal-transparency',
                            componentProps: {
                                index: i + 1
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    GaleriaPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_4__["TraductorService"] },
        { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"] }
    ]; };
    GaleriaPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-galeria',
            template: __webpack_require__(/*! raw-loader!./galeria.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/galeria/galeria.page.html"),
            styles: [__webpack_require__(/*! ./galeria.page.scss */ "./src/app/pages/galeria/galeria.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_4__["TraductorService"], src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"]])
    ], GaleriaPage);
    return GaleriaPage;
}());



/***/ })

}]);
//# sourceMappingURL=galeria-galeria-module-es5.js.map