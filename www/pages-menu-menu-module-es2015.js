(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-menu-menu-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/menu/menu.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/menu/menu.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-menu side=\"start\" contentId=\"sidemenu\">\n  <ion-header>\n    <ion-toolbar color=\"primary\">\n      <ion-buttons slot=\"end\">\n        <ion-button style=\"color: #fff;\" (click)=\"changeLanguage()\">{{translate.language}}</ion-button>\n      </ion-buttons>\n      <ion-title style=\"color: #fff;\">Menu</ion-title>\n    </ion-toolbar>\n  </ion-header>\n\n  <ion-content class=\"pattern-bg\">\n    <ion-toolbar>\n      <br>\n      <ion-title class=\"ion-text-center\">{{translate.appTitle}}</ion-title>\n      <br>\n      <div class=\"cool-line\"></div>\n    </ion-toolbar>\n    <br>\n    <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let page of pages\">\n      <ion-card\n        [routerLink]=\"page.url\" \n        routerDirection=\"root\" \n        class=\"animate__animated animate__fadeInLeft\"\n        [class.active-page]=\"selectedUrl === page.url\" \n        [class.deactive-page]=\"selectedUrl != page.url\" \n        style=\"margin-bottom: 25px;\">\n\n        <ion-item lines=\"none\" class=\"menu-button\">\n          <ion-icon [name]=\"page.icon\" slot=\"end\" color=\"light\"></ion-icon>\n          <ion-title>{{page.title}}</ion-title>\n        </ion-item>\n        <img class=\"menu-image\" *ngIf=\"selectedUrl === page.url\" [src]=\"page.image\">\n      </ion-card>\n    </ion-menu-toggle>\n  </ion-content>\n\n  <!-- <ion-footer>\n    <ion-toolbar>\n      <ion-icon color=\"primary\" name=\"pulse\" slot=\"start\" style=\"font-size: 30px; margin-left: 5%;\"></ion-icon>\n      <ion-icon color=\"primary\" name=\"pulse\" slot=\"end\" style=\"font-size: 30px; margin-right: 5%;\"></ion-icon>\n    </ion-toolbar>\n  </ion-footer> -->\n</ion-menu>\n\n<ion-router-outlet id=\"sidemenu\"></ion-router-outlet>\n"

/***/ }),

/***/ "./src/app/pages/menu/menu-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/menu/menu-routing.module.ts ***!
  \***************************************************/
/*! exports provided: MenuPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageRoutingModule", function() { return MenuPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./menu.page */ "./src/app/pages/menu/menu.page.ts");




const routes = [
    {
        path: 'menu',
        component: _menu_page__WEBPACK_IMPORTED_MODULE_3__["MenuPage"],
        children: [
            {
                path: 'inicio',
                loadChildren: () => __webpack_require__.e(/*! import() | inicio-inicio-module */ "inicio-inicio-module").then(__webpack_require__.bind(null, /*! ../inicio/inicio.module */ "./src/app/pages/inicio/inicio.module.ts")).then(m => m.InicioPageModule)
            },
            {
                path: 'galeria',
                loadChildren: () => __webpack_require__.e(/*! import() | galeria-galeria-module */ "galeria-galeria-module").then(__webpack_require__.bind(null, /*! ../galeria/galeria.module */ "./src/app/pages/galeria/galeria.module.ts")).then(m => m.GaleriaPageModule)
            },
            {
                path: 'contacto',
                loadChildren: () => __webpack_require__.e(/*! import() | contacto-contacto-module */ "contacto-contacto-module").then(__webpack_require__.bind(null, /*! ../contacto/contacto.module */ "./src/app/pages/contacto/contacto.module.ts")).then(m => m.ContactoPageModule)
            },
        ]
    },
    {
        path: '',
        redirectTo: '/menu/inicio',
        pathMatch: 'full'
    }
];
let MenuPageRoutingModule = class MenuPageRoutingModule {
};
MenuPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], MenuPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/menu/menu.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.module.ts ***!
  \*******************************************/
/*! exports provided: MenuPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPageModule", function() { return MenuPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./menu-routing.module */ "./src/app/pages/menu/menu-routing.module.ts");
/* harmony import */ var _menu_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./menu.page */ "./src/app/pages/menu/menu.page.ts");







let MenuPageModule = class MenuPageModule {
};
MenuPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _menu_routing_module__WEBPACK_IMPORTED_MODULE_5__["MenuPageRoutingModule"]
        ],
        declarations: [_menu_page__WEBPACK_IMPORTED_MODULE_6__["MenuPage"]]
    })
], MenuPageModule);



/***/ }),

/***/ "./src/app/pages/menu/menu.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/menu/menu.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL21lbnUvbWVudS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/menu/menu.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/menu/menu.page.ts ***!
  \*****************************************/
/*! exports provided: MenuPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MenuPage", function() { return MenuPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/traductor.service */ "./src/app/services/traductor.service.ts");




let MenuPage = class MenuPage {
    constructor(router, translate) {
        this.router = router;
        this.translate = translate;
        this.pages = [
            {
                title: this.translate.home,
                url: '/menu/inicio',
                icon: "home",
                image: 'assets/images/m1.jpg'
            },
            {
                title: this.translate.gallery,
                url: '/menu/galeria',
                icon: "images",
                image: 'assets/images/m2.jpg'
            },
            {
                title: this.translate.contact,
                url: '/menu/contacto',
                icon: "person",
                image: 'assets/images/m3.jpg'
            }
        ];
        this.selectedUrl = '/menu/inicio';
        this.router.events.subscribe((event) => {
            this.selectedUrl = event.url != null ? event.url : this.selectedUrl;
        });
    }
    ngOnInit() {
    }
    changeLanguage() {
        this.translate.changeLanguage();
        this.pages[0].title = this.translate.home;
        this.pages[1].title = this.translate.gallery;
        this.pages[2].title = this.translate.contact;
    }
};
MenuPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_3__["TraductorService"] }
];
MenuPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-menu',
        template: __webpack_require__(/*! raw-loader!./menu.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/menu/menu.page.html"),
        styles: [__webpack_require__(/*! ./menu.page.scss */ "./src/app/pages/menu/menu.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"], src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_3__["TraductorService"]])
], MenuPage);



/***/ })

}]);
//# sourceMappingURL=pages-menu-menu-module-es2015.js.map