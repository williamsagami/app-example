(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["inicio-inicio-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/inicio/inicio.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/inicio/inicio.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title style=\"color: #fff;\">{{translate.home}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"pattern-bg\">\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item lines=\"none\">\n      <img src=\"../assets/images/Logo.png\" style=\"margin-left: auto; margin-right: auto; margin-top: 12px; margin-bottom: 12px; height: 160px; width: 160px;\">\n    </ion-item>\n  </ion-card>\n\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-slides pager=\"true\" [options]=\"slideOpts\" autoplay=\"true\" loop=\"true\" class=\"slider\">\n      <ion-slide *ngFor=\"let image of data.sliderImages\">\n        <img [src]=\"image\">\n      </ion-slide>\n    </ion-slides>\n  </ion-card>\n\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item>\n      <ion-icon slot=\"end\" name=\"book\" color=\"primary\"></ion-icon>\n      <ion-title style=\"margin: 12px; margin-left: 0px;\">{{translate.appTitle}}</ion-title>\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-card-content>\n        {{translate.description}}\n      </ion-card-content>\n    </ion-item>\n    <img class=\"image\" src=\"../assets/images/l1.jpg\" style=\"height: 150px;\">\n  </ion-card>\n\n  <ion-card class=\"cool-card\">\n    <ion-item lines=\"none\" class=\"cool-item\">\n      <ion-icon slot=\"end\" name=\"eye\" color=\"light\"></ion-icon>\n      <ion-title>{{translate.video}}</ion-title>\n    </ion-item>\n\n    <video style=\"width: 100%; height: 400px; background-color: white;\" controls>\n      <source src=\"../assets/videos/1.mp4#t=1\" type=\"video/mp4\">\n      Your browser does not support HTML5 video.\n    </video>\n  </ion-card>\n\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item lines=\"none\">\n      <img src=\"../assets/images/f.png\" style=\"height: 300px; width: 300px;\">\n    </ion-item>\n  </ion-card>\n\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-item lines=\"none\" class=\"cool-item\">\n      <ion-icon slot=\"end\" name=\"settings\" color=\"light\"></ion-icon>\n      <ion-title>{{translate.procedures}}</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <ion-item *ngFor=\"let procedure of translate.procedureList\">\n      <ion-icon slot=\"end\" name=\"checkmark\" color=\"primary\"></ion-icon>\n      <ion-label>{{procedure}}</ion-label>\n    </ion-item>\n  </ion-card>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/inicio/inicio-routing.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/inicio/inicio-routing.module.ts ***!
  \*******************************************************/
/*! exports provided: InicioPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioPageRoutingModule", function() { return InicioPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _inicio_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./inicio.page */ "./src/app/pages/inicio/inicio.page.ts");




var routes = [
    {
        path: '',
        component: _inicio_page__WEBPACK_IMPORTED_MODULE_3__["InicioPage"]
    }
];
var InicioPageRoutingModule = /** @class */ (function () {
    function InicioPageRoutingModule() {
    }
    InicioPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
        })
    ], InicioPageRoutingModule);
    return InicioPageRoutingModule;
}());



/***/ }),

/***/ "./src/app/pages/inicio/inicio.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/inicio/inicio.module.ts ***!
  \***********************************************/
/*! exports provided: InicioPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioPageModule", function() { return InicioPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _inicio_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./inicio-routing.module */ "./src/app/pages/inicio/inicio-routing.module.ts");
/* harmony import */ var _inicio_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./inicio.page */ "./src/app/pages/inicio/inicio.page.ts");







var InicioPageModule = /** @class */ (function () {
    function InicioPageModule() {
    }
    InicioPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _inicio_routing_module__WEBPACK_IMPORTED_MODULE_5__["InicioPageRoutingModule"]
            ],
            declarations: [_inicio_page__WEBPACK_IMPORTED_MODULE_6__["InicioPage"]]
        })
    ], InicioPageModule);
    return InicioPageModule;
}());



/***/ }),

/***/ "./src/app/pages/inicio/inicio.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/inicio/inicio.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2luaWNpby9pbmljaW8ucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/inicio/inicio.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/inicio/inicio.page.ts ***!
  \*********************************************/
/*! exports provided: InicioPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioPage", function() { return InicioPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/traductor.service */ "./src/app/services/traductor.service.ts");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");




var InicioPage = /** @class */ (function () {
    function InicioPage(translate, data) {
        this.translate = translate;
        this.data = data;
        this.slideOpts = {
            loop: true,
            autoplay: {
                delay: 2000,
            }
        };
    }
    InicioPage.prototype.ngOnInit = function () {
    };
    InicioPage.ctorParameters = function () { return [
        { type: src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_2__["TraductorService"] },
        { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"] }
    ]; };
    InicioPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-inicio',
            template: __webpack_require__(/*! raw-loader!./inicio.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/inicio/inicio.page.html"),
            styles: [__webpack_require__(/*! ./inicio.page.scss */ "./src/app/pages/inicio/inicio.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_2__["TraductorService"], src_app_services_data_service__WEBPACK_IMPORTED_MODULE_3__["DataService"]])
    ], InicioPage);
    return InicioPage;
}());



/***/ })

}]);
//# sourceMappingURL=inicio-inicio-module-es5.js.map