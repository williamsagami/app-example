(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["contacto-contacto-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/contacto/contacto.page.html":
/*!*****************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/contacto/contacto.page.html ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar color=\"primary\">\n    <ion-buttons slot=\"start\">\n      <ion-menu-button style=\"color: #fff;\"></ion-menu-button>\n    </ion-buttons>\n    <ion-title style=\"color: #fff;\">{{translate.contact}}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content class=\"pattern-bg\">\n\n  <!-- TELEFONO -->\n  <ion-card class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item>\n      <ion-icon slot=\"end\" name=\"call\" color=\"primary\"></ion-icon>\n      <ion-title>{{translate.phones}}</ion-title>\n    </ion-item>\n\n    <!-- <br>\n    <ion-card-subtitle style=\"margin-left: 5%;\">Celular:</ion-card-subtitle>\n    <ion-item>\n      <ion-label>664 682 8834</ion-label>\n    </ion-item>\n    <ion-item>\n      <ion-label></ion-label>\n    </ion-item>\n    <br>\n    <ion-card-subtitle style=\"margin-left: 5%;\">Telefono:</ion-card-subtitle> -->\n    <ion-item lines=\"none\" *ngFor=\"let phone of data.phones\">\n      <ion-label>{{phone.number}}</ion-label>\n    </ion-item>\n\n    <ion-item lines=\"none\" class=\"cool-item\" button (click)=\"showNumberList($event)\">\n      <ion-label style=\"text-align: center;\">{{translate.call}}</ion-label>\n      <ion-icon name=\"call\" color=\"light\"></ion-icon>\n    </ion-item>\n  </ion-card>\n\n  <!-- DIRECCION -->\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-item>\n      <ion-icon slot=\"end\" name=\"locate\" color=\"primary\"></ion-icon>\n      <ion-title>{{translate.direction}}</ion-title>\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-card-content>\n        {{data.direction}}\n      </ion-card-content>\n    </ion-item>\n  </ion-card>\n  \n  <!-- MAPA -->\n  <ion-card  class=\"animate__animated animate__fadeInLeft cool-card\">\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"end\" name=\"map\" color=\"primary\"></ion-icon>\n      <ion-title>{{translate.location}}</ion-title>\n    </ion-item>\n\n    <div class=\"cool-line\"></div>\n\n    <iframe style=\"width: 100%; height: 300px; background-color: #ffffff;\"\n      src= 'https://maps.google.com/maps?q=Calle%204ta%2C%20Diaz%20Miron%208064%2C%20Zona%20Centro&t=&z=15&ie=UTF8&iwloc=&output=embed'\n      frameborder=\"0\" scrolling=\"no\" marginheight=\"0\" marginwidth=\"0\"></iframe>\n    <div\n      style=\"position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;\">\n      <small style=\"line-height: 1.8;font-size: 2px;background: #fff;\">\n        Map by <a href=\"https://www.googlemapsembed.net/\" rel=\"nofollow\">Embed Google Maps</a>\n      </small>\n    </div>\n    <style>\n      #gmap_canvas img {\n        max-width: none !important;\n        background: none !important\n      }\n    </style>\n  </ion-card >\n  <!-- Embed code -->\n  <script type=\"text/javascript\">(new Image).src = \"//googlemapsembed.net/get?r\" + escape(document.referrer);</script>\n  <script type=\"text/javascript\" src=\"https://googlemapsembed.net/embed\"></script>\n  <!-- END CODE -->\n  \n  <!-- REDES SOCIALES -->\n  <ion-card class=\"animate__animated animate__fadeInRight cool-card\">\n    <ion-item >\n      <ion-icon slot=\"end\" name=\"people\" color=\"primary\"></ion-icon>\n      <ion-title>{{translate.social}}</ion-title>\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"start\" name=\"mail\" color=\"primary\"></ion-icon>\n      <ion-label>dentalsmarttijuana@gmail.com</ion-label>\n    </ion-item>\n\n    <ion-item lines=\"none\">\n      <ion-icon slot=\"start\" name=\"mail\" color=\"primary\"></ion-icon>\n      <ion-label>sheridan.cortez@gmail.com</ion-label>\n    </ion-item>\n\n    <!-- <ion-item class=\"cool-item\" button (click)=\"openLink('https://dentalcosmeticstud.wixsite.com/dentalcosmeticstudio?fbclid=IwAR2tQIoYwZYjEpYh6TebTTki4KLwgTUKmgPdsZV1jaVSBMJyYa2_laDS4yw')\">\n      <ion-icon slot=\"start\" name=\"globe\" color=\"light\"></ion-icon>\n      <ion-label>dentalcosmeticstud.wixsite.com</ion-label>\n      <ion-icon slot=\"end\" name=\"arrow-forward\" color=\"light\"></ion-icon>\n    </ion-item> -->\n\n    <ion-item lines=\"none\" class=\"cool-item\" button (click)=\"openLink('https://www.facebook.com/dentalsmartt')\">\n      <ion-icon slot=\"start\" name=\"logo-facebook\" color=\"light\"></ion-icon>\n      <ion-label>Dental Smart</ion-label>\n      <ion-icon slot=\"end\" name=\"arrow-forward\" color=\"light\"></ion-icon>\n    </ion-item>\n\n    <ion-item lines=\"none\" class=\"cool-item\" button (click)=\"openLink('https://www.instagram.com/dentalsmarttijuana/')\">\n      <ion-icon slot=\"start\" name=\"logo-instagram\" color=\"light\"></ion-icon>\n      <ion-label>dentalsmarttijuana</ion-label>\n      <ion-icon slot=\"end\" name=\"arrow-forward\" color=\"light\"></ion-icon>\n    </ion-item>\n  </ion-card>\n</ion-content>"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/numberlist/numberlist.component.html":
/*!**************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/numberlist/numberlist.component.html ***!
  \**************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-card class=\"cool-card\" style=\"margin: 0px; border-radius: 10px; border-width: 2px;\">\n  <ion-list>\n    <ion-item lines=\"none\" *ngFor=\"let phone of data.phones\">\n      <ion-label (click)=\"phoneCall(phone.call)\">{{phone.number}}</ion-label>\n      <ion-icon slot=\"end\" name=\"arrow-forward\" color=\"primary\"></ion-icon>\n    </ion-item>\n  </ion-list>\n</ion-card>\n\n"

/***/ }),

/***/ "./src/app/pages/contacto/contacto-routing.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/contacto/contacto-routing.module.ts ***!
  \***********************************************************/
/*! exports provided: ContactoPageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageRoutingModule", function() { return ContactoPageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/contacto/contacto.page.ts");




const routes = [
    {
        path: '',
        component: _contacto_page__WEBPACK_IMPORTED_MODULE_3__["ContactoPage"]
    }
];
let ContactoPageRoutingModule = class ContactoPageRoutingModule {
};
ContactoPageRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]],
    })
], ContactoPageRoutingModule);



/***/ }),

/***/ "./src/app/pages/contacto/contacto.module.ts":
/*!***************************************************!*\
  !*** ./src/app/pages/contacto/contacto.module.ts ***!
  \***************************************************/
/*! exports provided: ContactoPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPageModule", function() { return ContactoPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./contacto-routing.module */ "./src/app/pages/contacto/contacto-routing.module.ts");
/* harmony import */ var _contacto_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./contacto.page */ "./src/app/pages/contacto/contacto.page.ts");
/* harmony import */ var _numberlist_numberlist_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../numberlist/numberlist.component */ "./src/app/pages/numberlist/numberlist.component.ts");








let ContactoPageModule = class ContactoPageModule {
};
ContactoPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
            _contacto_routing_module__WEBPACK_IMPORTED_MODULE_5__["ContactoPageRoutingModule"]
        ],
        entryComponents: [_numberlist_numberlist_component__WEBPACK_IMPORTED_MODULE_7__["NumberlistComponent"]],
        declarations: [_contacto_page__WEBPACK_IMPORTED_MODULE_6__["ContactoPage"], _numberlist_numberlist_component__WEBPACK_IMPORTED_MODULE_7__["NumberlistComponent"]]
    })
], ContactoPageModule);



/***/ }),

/***/ "./src/app/pages/contacto/contacto.page.scss":
/*!***************************************************!*\
  !*** ./src/app/pages/contacto/contacto.page.scss ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2NvbnRhY3RvL2NvbnRhY3RvLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/contacto/contacto.page.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/contacto/contacto.page.ts ***!
  \*************************************************/
/*! exports provided: ContactoPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ContactoPage", function() { return ContactoPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/in-app-browser/ngx */ "./node_modules/@ionic-native/in-app-browser/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _numberlist_numberlist_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../numberlist/numberlist.component */ "./src/app/pages/numberlist/numberlist.component.ts");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");
/* harmony import */ var src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/traductor.service */ "./src/app/services/traductor.service.ts");







let ContactoPage = class ContactoPage {
    constructor(iab, poc, translate, data) {
        this.iab = iab;
        this.poc = poc;
        this.translate = translate;
        this.data = data;
    }
    ngOnInit() {
    }
    openLink(link) {
        this.iab.create(link);
    }
    showNumberList(ev) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.poc.create({
                component: _numberlist_numberlist_component__WEBPACK_IMPORTED_MODULE_4__["NumberlistComponent"],
                event: ev,
                mode: 'ios',
            });
            return yield popover.present();
        });
    }
};
ContactoPage.ctorParameters = () => [
    { type: _ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"] },
    { type: src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_6__["TraductorService"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"] }
];
ContactoPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-contacto',
        template: __webpack_require__(/*! raw-loader!./contacto.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/contacto/contacto.page.html"),
        styles: [__webpack_require__(/*! ./contacto.page.scss */ "./src/app/pages/contacto/contacto.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_in_app_browser_ngx__WEBPACK_IMPORTED_MODULE_2__["InAppBrowser"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["PopoverController"], src_app_services_traductor_service__WEBPACK_IMPORTED_MODULE_6__["TraductorService"], src_app_services_data_service__WEBPACK_IMPORTED_MODULE_5__["DataService"]])
], ContactoPage);



/***/ }),

/***/ "./src/app/pages/numberlist/numberlist.component.scss":
/*!************************************************************!*\
  !*** ./src/app/pages/numberlist/numberlist.component.scss ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL251bWJlcmxpc3QvbnVtYmVybGlzdC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/numberlist/numberlist.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/pages/numberlist/numberlist.component.ts ***!
  \**********************************************************/
/*! exports provided: NumberlistComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NumberlistComponent", function() { return NumberlistComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _llamada_llamada_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../llamada/llamada.page */ "./src/app/pages/llamada/llamada.page.ts");
/* harmony import */ var src_app_services_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/data.service */ "./src/app/services/data.service.ts");





let NumberlistComponent = class NumberlistComponent {
    constructor(modalController, poc, data) {
        this.modalController = modalController;
        this.poc = poc;
        this.data = data;
    }
    ngOnInit() { }
    phoneCall(phoneNumber) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _llamada_llamada_page__WEBPACK_IMPORTED_MODULE_3__["LlamadaPage"],
                componentProps: {
                    number: phoneNumber
                }
            });
            yield modal.present();
            console.log(modal.componentProps.number);
            this.poc.dismiss();
        });
    }
};
NumberlistComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"] },
    { type: src_app_services_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"] }
];
NumberlistComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-numberlist',
        template: __webpack_require__(/*! raw-loader!./numberlist.component.html */ "./node_modules/raw-loader/index.js!./src/app/pages/numberlist/numberlist.component.html"),
        styles: [__webpack_require__(/*! ./numberlist.component.scss */ "./src/app/pages/numberlist/numberlist.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["PopoverController"], src_app_services_data_service__WEBPACK_IMPORTED_MODULE_4__["DataService"]])
], NumberlistComponent);



/***/ })

}]);
//# sourceMappingURL=contacto-contacto-module-es2015.js.map